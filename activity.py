# Create 5 variables
name = "John Kenneth Bautista"
age = 19
job_title = "database administrator"
movie = "Pangarap kong holdap"
rating = 97.99
print(f"I am {name}, and I am {age} years old, I work as a {job_title}, and my rating for {movie} is {rating}%")

# Create 3 variables
num1, num2, num3 = 5, 10, 15
product = num1 * num2
check_highest_value = num1 < num3
sum_result = num3 + num2
print(product)
print(check_highest_value)
print(sum_result)